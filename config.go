package goconfig

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"reflect"
	"strconv"
	"strings"

	"gopkg.in/yaml.v2"
)

// LoadFromBytes загружает конфиг из байтов в структуру.
func LoadFromBytes(data []byte, config interface{}) (err error) {
	return yaml.Unmarshal(data, config)
}

// LoadFromString загружает конфиг из строки в структуру.
func LoadFromString(data string, config interface{}) (err error) {
	return LoadFromBytes([]byte(data), config)
}

// LoadFromFile - загружает конфиг из файла в структуру.
func LoadFromFile(configPath string, config interface{}) (err error) {
	if configPath == "" {
		err = errors.New("empty configuration file path")
		return
	}

	configData, err := ioutil.ReadFile(configPath)
	if err != nil {
		return
	}

	return yaml.Unmarshal(configData, config)
}

// LoadFromEnv переопределяет значения конфига для которых задан таг env и задана соответсвующая переменная окружения
func LoadFromEnv(config interface{}) (err error) {
	err = overrideFieldsValue(config)
	if err != nil {
		err = fmt.Errorf("load from env error: %v", err)
		return
	}

	return
}

func overrideFieldsValue(object interface{}) (err error) {
	if object == nil {
		return
	}

	s := reflect.Indirect(reflect.ValueOf(object))
	for i := 0; i < s.NumField(); i++ {
		structField := s.Type().Field(i)
		field := s.Field(i)
		if strings.Title(structField.Name) != structField.Name {
			continue
		}

		switch field.Kind() {
		case reflect.Ptr:
			if !field.IsNil() {
				if field.Elem().Kind() == reflect.Struct {
					err = overrideFieldsValue(field.Interface())
					if err != nil {
						return
					}
				}
			}
		case reflect.Struct:
			err = overrideFieldsValue(field.Interface())
			if err != nil {
				return
			}
		}

		err = setStructField(structField, field)
		if err != nil {
			return
		}
	}

	return
}

func setStructField(structField reflect.StructField, value reflect.Value) (err error) {
	envName, ok := structField.Tag.Lookup("env")
	if !ok {
		return
	}

	if !value.CanSet() {
		err = fmt.Errorf("can't set: %v by env: %v (unadressable)", structField.Name, envName)
		return
	}

	envValue, ok := os.LookupEnv(envName)
	if !ok {
		return
	}

	switch structField.Type.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64,
		reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:

		val, err := strconv.ParseInt(envValue, 10, 64)
		if err == nil {
			value.SetInt(val)
		}
	case reflect.String:
		value.SetString(envValue)
	case reflect.Float32, reflect.Float64:
		val, err := strconv.ParseFloat(envValue, 64)
		if err == nil {
			value.SetFloat(val)
		}
	case reflect.Bool:
		val, err := strconv.ParseBool(envValue)
		if err == nil {
			value.SetBool(val)
		}
	default:
		err = fmt.Errorf(
			"not implemented string to %s conversion from environment variable: %s=%s",
			structField.Type.Name(),
			structField.Name,
			envValue,
		)
		return

	}
	return
}
